#ifndef _TYPES_H
#define _TYPES_H

#include <stdlib.h>

#define DEFAULT_INDEX_TABLE_SIZE 8

// Данные
typedef struct
{
    float key[3];       // Ключевое поле
    int vector1[3];     // Вектор 1
    int vector2[3];     // Вектор 2
    char flag;          // Флаг

} data_t;

// Узел списка
typedef struct list_node_struct
{
    struct list_node_struct * _prev;     // Указатель на предыдущий элемент
    struct list_node_struct * _next;     // Указатель на последующий элемент

    data_t data;                         // Данные

} list_node_t;

// Индексная таблица
// Примечания:
// Элемент [] может указывать на любой элемент
typedef struct
{
    struct list_node_struct * ptr;   // Указатель на один элемент списка
    size_t index;                    // Соответствующий индекс

} index_table_t;

// Индексированный список
typedef struct
{
    index_table_t * _table;     // Массив записей таблицы
    size_t _in_table_count;     // Размер таблицы
    size_t _count;                 // Число элементов

} ilist_t;

#endif

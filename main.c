#include <assert.h>
#include <locale.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "types.h"

bool ilIsValid(ilist_t * ilist);

inline static void memswap(void * restrict __first, void * restrict __second, size_t __n)
{
    void * __buff = malloc(__n);
    memcpy(__buff, __first, __n);
    memcpy(__first, __second, __n);
    memcpy(__second, __buff, __n);
    free(__buff);
}

// Создание узла списка
inline static list_node_t * mklNode(data_t * data)
{
    list_node_t * list_node = malloc(sizeof(list_node_t));

    list_node->_prev = NULL;
    list_node->_next = NULL;

    memcpy(&list_node->data, data, sizeof(data_t));

    return list_node;
}

// Базовая инициализация структуры
ilist_t * mkIndexedList(size_t in_table_count)
{
    // Проверка размера таблицы
    assert(in_table_count > 0);

    // Выделение памяти
    ilist_t * ilist = malloc(sizeof(ilist_t));

    // Инициализация
    ilist->_table = malloc(in_table_count * sizeof(index_table_t));
    ilist->_in_table_count = in_table_count;
    ilist->_count = 0;

    // Зануление таблицы
    for (size_t i = 0; i < in_table_count; ++i)
    {
        ilist->_table[i].ptr = NULL;
        ilist->_table[i].index = 0;
    }
    
    return ilist;
}

// Очистка структуры
void ilclear(ilist_t * ilist)
{
    assert(ilIsValid(ilist));

    list_node_t * node = ilist->_table[0].ptr;
    if (node)
    {
        // Очищаем память листов
        while (node->_next != NULL)
        {
            node = node->_next;
            free(node->_prev);
        }
        // Очищаем таблицу
        for (size_t i = 0; i < ilist->_in_table_count; ++i)
        {
            ilist->_table[i].ptr = NULL;
            ilist->_table[i].index = 0;
        }
    }

    ilist->_count = 0;

    assert(ilIsValid(ilist));
}

// Удаление структуры
void ilfree(ilist_t * ilist)
{
    assert(ilist);

    ilclear(ilist);
    free(ilist);
}

// Размер заполненной части таблицы
// (Если таблица заполнена не полностью, если число элементов меньше размерности таблицы)
inline static size_t ilTableCount(ilist_t * ilist)
{
    return (ilist->_in_table_count < ilist->_count)
            ? ilist->_in_table_count
            : ilist->_count;
}

// Проверка на валидность таблицы
bool ilIsValid(ilist_t * ilist)
{   
    bool result = true;

    // Проверка на существование структуры
    if (ilist && ilist->_table)
    {
        index_table_t * table_record = ilist->_table;       // Итератор по таблице
        list_node_t * node = ilist->_table->ptr;            // Итератор по списку

        size_t max_table_i = ilTableCount(ilist) - 1;       // Индекс последней записи в таблице

        if (node)
        {
            // Проверка первого элемента
            // (Первый элемент не должен иметь предыдущих)
            if (node->_prev != NULL)
            {
                result = false;
            }

            // Проверка корректности индексов в таблице
            for (size_t i = 0; i < ilist->_count; ++i)
            {
                // Проверка участка таблицы
                if (table_record->index == i)
                {
                    // Поиск ошибок в таблице
                    if (table_record->ptr != node)
                    {
                        result = false;
                        break;
                    }

                    // Следующая итерация
                    if (table_record < &ilist->_table[max_table_i + 1])
                    {
                        ++table_record;
                    }
                    // Если таблица закончилась
                    else
                    {
                        break;
                    }
                }

                if (node)
                {
                    node = node->_next;
                }
            }

            // Проверка последнего элемента
            // (Последний элемент не должен содержать последующих)
            list_node_t * last = ilist->_table[max_table_i].ptr;
            if (last->_next != NULL)
            {
                result = false;
            }

            // Проверка на упорядоченность и отсутствие дубликатов
            int prev_index = -1;
            for (index_table_t * cur = ilist->_table; cur < &ilist->_table[max_table_i + 1]; ++cur)
            {
                if (prev_index >= (int)cur->index)
                {
                    result = false;
                    break;
                }

                prev_index = (int)cur->index;
            }
        }
        // Для пустого списка
        else
        {
            if (ilist->_count != 0)
            {
                result = false;
            }
        }
    }
    // Указатель на NULL вместо структуры или таблицы
    else
    {
        result = false;
    }

    return result;
}

// Перестройка индексной таблицы.
// Для работы функции требуется корректность первого и последнего элемента индексной таблицы,
// а также их количества и физического размера индексной таблицы.
/*
void ilRebuildTable(ilist_t * ilist) // Don`t used
{
    assert(ilist);
    assert(ilist->_table);

    list_node_t * node = ilist->_table[0].ptr;  // Узел (пока первый)
    size_t table_i = 1;                         // Индекс таблицы индексов

    // Обход по таблице
    for (size_t list_i = 1; list_i < ilist->_count; ++list_i)
    {
        if (list_i % ilTableCount(ilist) - 1) // Перезапись таблицы
        {
            ilist->_table[table_i].index = list_i;
            ilist->_table[table_i].ptr = node;

            ++table_i;
        }

        node = node->_next;
    }
}
*/

// Связь двух узлов
static void lnlink(list_node_t * ilist1, list_node_t * ilist2)
{
    if (ilist1) ilist1->_next = ilist2;
    if (ilist2) ilist2->_prev = ilist1;
}

// Поиск номера узла в списке по значению
int ilindex(ilist_t * ilist, data_t * data)
{
    // Получаем первый элемент
    list_node_t * node = ilist->_table[0].ptr;

    for   (size_t i = 0; node->_next != NULL; ++i)
    {
        if (
                node->data.key[0]       == data->key[0]      &&
                node->data.key[1]       == data->key[1]      &&
                node->data.key[2]       == data->key[2]      &&
                node->data.vector1[0]   == data->vector1[0]  &&
                node->data.vector1[2]   == data->vector1[2]  &&
                node->data.vector1[1]   == data->vector1[1]  &&
                node->data.vector2[0]   == data->vector2[0]  &&
                node->data.vector2[1]   == data->vector2[1]  &&
                node->data.vector2[2]   == data->vector2[2]  &&
                node->data.flag         == data->flag
                )
        {
            return i;
        }

        node = node->_next;
    }

    return -1;
}

// Поиск ближайшейго индекса таблицы для индекса в списке
int ilFindTablePosition(ilist_t * ilist, size_t position)
{
    index_table_t * table = ilist->_table;      // Индексная таблица
    int table_i = 0;                            // Индекс блежаешего элемента из таблицы
    int min_len = position;                     // Длина пути до первого элемента

    // Поиск позиции в таблице
    for (size_t i = 1; i < ilTableCount(ilist); ++i)
    {
        int len = position - table[i].index;

        if (abs(len) < abs(min_len))
        {
            min_len = len;
            table_i = i;
        }
        else
        {
            break;
        }
    }

    return table_i;
}

// Поиск узла в списке по номеру позиции от выбранного узла индексной таблицы
list_node_t * ilfindfast(ilist_t * ilist, size_t position, int table_i)
{
    // Проверяем корректность списка
    assert(ilIsValid(ilist));

    // Проверяем корректность индекса
    bool is_correct_table_i = table_i >= 0 && table_i < (int)ilTableCount(ilist);
    assert(is_correct_table_i);
    assert(0 <= position && position < ilist->_count);

    index_table_t * table = ilist->_table;          // Индексная таблица
    int len = position - table[table_i].index;      // Длина пути до первого элемента
    list_node_t * node = table[table_i].ptr;        // Итератор по списку

    // Приходим в нужную точку
    if (len > 0)
    {
        for (size_t i = table[table_i].index; i != position; ++i)
        {
            node = node->_next;     // Переход дальше
        }
    }
    if (len < 0)
    {
        for (size_t i = table[table_i].index; i != position; --i)
        {
            node = node->_prev;     // Переход назад
        }
    }

    return node;
}

// Поиск узла в списке по номеру позиции
list_node_t * ilfind(ilist_t * ilist, size_t position)
{
    return ilfindfast(ilist, position, ilFindTablePosition(ilist, position));
}

// Удаление узла из позиции
void ilremove(ilist_t * ilist, size_t position)
{
    // Проверяем корректность списка
    assert(ilIsValid(ilist));

    index_table_t * table = ilist->_table;                              // Таблица индексов
    size_t table_i = ilFindTablePosition(ilist, position);              // Индекс ближайшей позиции из таблицы
    list_node_t * removed = ilfindfast(ilist, position, table_i);       // Удаляемый элемент

    // Сокращаем таблицу, если необходимо
    if (ilist->_count - 1 <= ilist->_in_table_count)
    {
        // Если список пустеет добавляем пустой указатель
        if (ilist->_count - 1 == 0)
        {
            table[0].index = 0;
            table[0].ptr = NULL;
        }
        // Затираем ненужный участок таблицы
        else
        {
            for (size_t i = table_i; i < ilTableCount(ilist) - 1; ++i)
            {
                memcpy(&table[i], &table[i + 1], sizeof(index_table_t));
            }
        }
    }
    // Правим таблицу если удаляемый элемент находится в таблице
    else if (position == table[table_i].index)
    {
        if (position > table_i)
        {
            for (size_t i = table_i; i > 0; --i)
            {
                // Смещаем данные записи индексной таблицы влево
                table[i].ptr = table[i].ptr->_prev;
                --table[i].index;

                if (table[i - 1].index != table[i].index)
                {
                    break;
                }
            }
        }
        else
        {
            for (size_t i = table_i; i < ilTableCount(ilist) - 1; ++i)
            {
                // Смещаем данные записи индексной таблицы вправо
                table[i].ptr = table[i].ptr->_next;
                ++table[i].index;

                if (table[i + 1].index != table[i].index)
                {
                    break;
                }
            }
        }

        assert(ilIsValid(ilist));   // Проверяем корректность списка
    }

    // Удаляем элемент
    --ilist->_count;
    lnlink(removed->_prev, removed->_next);
    free(removed);

    // Проверяем направление
    table_i = (position > table[table_i].index)
            ? table_i + 1
            : table_i;

    // Уменьшаем индексы последующих элементов
    for (size_t i = table_i; i < ilTableCount(ilist); ++i)
    {
        --ilist->_table[i].index;
    }

    assert(ilIsValid(ilist));
}

// Вставка в начало
void ilappbegin(ilist_t * ilist, data_t * data)
{
    assert(ilIsValid(ilist));
    assert(data);

    index_table_t * table = ilist->_table;  // Индексная таблица
    list_node_t * pasted = mklNode(data);   // Вставляемый блок
    lnlink(pasted, table[0].ptr);           // Цепляем список к новому элементу

    // Вставка в случае если размер таблицы не максимален
    if (ilist->_count < ilist->_in_table_count)
    {
        index_table_t table_record;                                 // Временное хранилище для записей таблицы
        memcpy(&table_record, &table[0], sizeof(index_table_t));    // Помещаем сюда старую первую запись

        table[0].ptr = pasted;
        table[0].index = 0;

        for (size_t table_i = 0; table_i < ilTableCount(ilist); ++table_i)
        {
            ++table_record.index;                                                   // Увеличиваем индекс
            memswap(&table[table_i + 1], &table_record, sizeof(index_table_t));     // Обмениваем данные в участке памяти
        }
    }
    // Вставка в случае, если число элементов больше размера таблицы
    else
    {
        table[0].ptr = pasted;
        table[0].index = 0;

        for (size_t table_i = 1; table_i < ilTableCount(ilist); ++table_i)
        {
            ++table[table_i].index;     // Увеличиваем индексы
        }
    }

    ++ilist->_count;            // Увеличиваем число элементов в списке
    assert(ilIsValid(ilist));   // Проверяем валидность списка
}

// Вставка в конец
void ilappend(ilist_t * ilist, data_t * data)
{
    assert(ilIsValid(ilist));
    assert(data);

    index_table_t * table = ilist->_table;              // Индексная таблица
    size_t last_record_i = ilTableCount(ilist) - 1;     // Индекс последней записи в таблице
    list_node_t * pasted = mklNode(data);               // Вставляемый блок
    lnlink(table[last_record_i].ptr, pasted);           // Цепляем новому элемент к списку

    // Расширяем таблицу, если число элементов меньше допустимого размера индексной таблицы
    if (ilist->_count < ilist->_in_table_count)
    {
        ++last_record_i;
    }

    // Последняя запись
    table[last_record_i].index = ilist->_count;
    table[last_record_i].ptr = pasted;

    ++ilist->_count;            // Увеличиваем число элементов в списке
    assert(ilIsValid(ilist));   // Проверяем валидность списка
}

// Добавление в произвольную позицию списка
void ilinsert(ilist_t * ilist, data_t * data, size_t position)
{
    assert(ilIsValid(ilist));
    assert(position >= 0 && position < ilist->_count);
    assert(data);

    index_table_t * table = ilist->_table;  // Индексная таблица
    list_node_t * pasted = mklNode(data);   // Вставляемый элемент

    size_t table_i = ilFindTablePosition(ilist, position);          // Индекс ближайшей записи
    list_node_t * target = ilfindfast(ilist, position, table_i);    // Узел после которого необходимо вставить узел

    lnlink(target->_prev, pasted);                                  // Внедрение узла
    lnlink(pasted, target);                                         // ^

    // Получаем индекс таблицы, после которой необходимо увеличивать индекс
    table_i = (position > table[table_i].index)
            ? table_i + 1
            : table_i;

    // Вставка в случае если размер таблицы не максимален
    if (ilist->_count < ilist->_in_table_count)
    {
        // Создаём хранилище для нового элемента
        index_table_t table_record;
        table_record.ptr = pasted;
        table_record.index = position;

        memswap(&table_record, &table[table_i], sizeof(index_table_t));         // Обмениваемся со старой первой записью

        for (size_t i = table_i; i < ilTableCount(ilist); ++i)
        {
            ++table_record.index;                                               // Увеличиваем индекс
            memswap(&table[i + 1], &table_record, sizeof(index_table_t));       // Обмениваем данные в участке памяти
        }
    }
    // Вставка в случае, если число элементов больше размера таблицы
    else
    {
        // Вставка нового элемента виндексную таблицу, если необходимо
        if (position == table[table_i].index && table_i < ilTableCount(ilist) - 1)
        {
            table[table_i].ptr = pasted;
            table[table_i].index = position;

            ++table_i;
        }

        for (size_t i = table_i; i < ilTableCount(ilist); ++i)
        {
            ++table[i].index;     // Увеличиваем индексы
        }
    }

    ++ilist->_count;            // Увеличиваем число элементов в списке

    assert(ilIsValid(ilist));   // Проверяем валидность списка
}

// Копия объекта
data_t * ilget(ilist_t * ilist, size_t position)
{
    assert(ilist);

    list_node_t * node = ilfind(ilist, position);

    data_t * result = malloc(sizeof(data_t));
    memcpy(result, &node->data, sizeof(data_t));

    return result;
}

// Установить значение узла
void ilset(ilist_t * ilist, size_t position, data_t * data)
{
    assert(ilist);
    assert(data);

    list_node_t * node = ilfind(ilist, position);
    if (node)
    {
        memcpy(&node->data, data, sizeof(data_t));
    }
}

// Печать структуры списка
void ilStructPrint(ilist_t * ilist)
{
    printf("Virtual index table size = %zu\n", ilTableCount(ilist));

    for (size_t i = 0; i < ilist->_in_table_count; ++i)
    {
        printf("%zu: index = %zu, ptr = 0x%x\n", i, ilist->_table[i].index, (unsigned)ilist->_table[i].ptr);
    }
}

// Печать элементов последовательно
void ilprint(ilist_t * ilist)
{
    list_node_t * node = ilist->_table[0].ptr;
    if (node != NULL)
    {
        for (size_t i = 0; node->_next != NULL; ++i)
        {
            printf("%zu)\n"
                   "\tkey: %.0f, %.0f, %.0f\n"
                   "\tvector1: %d, %d, %d\n"
                   "\tvector2: %d, %d, %d\n"
                   "\tflag: %c;\n",
                   i,
                   node->data.key[0],
                   node->data.key[1],
                   node->data.key[2],
                   node->data.vector1[0],
                   node->data.vector1[2],
                   node->data.vector1[1],
                   node->data.vector2[0],
                   node->data.vector2[1],
                   node->data.vector2[2],
                   node->data.flag
                   );
            node = node->_next;
        }
    }
}

// Генерация некоторых данных
data_t * genDataArray(int count)
{
    data_t * source = calloc(count, sizeof(data_t));

    for (int i = 0; i < count; ++i)
    {
        for (size_t j = 0; j < 3; ++j)
        {
            source[i].key[j]		= (float)(rand() % 8) / 1.0;
            source[i].vector1[j]	= rand() % 128;
            source[i].vector2[j]	= rand() % 128;
        }

        source[i].flag = rand() % ('z' - 'a' + 1) + 'a';
    }

    return source;
}

int main()
{
    srand(time(NULL));
    setlocale(LC_ALL, "ru");
    puts("Начало программы");

    data_t * source = genDataArray(40);

    puts("Генерируем список значений\n");
    ilist_t * ilist = mkIndexedList(8);

    puts("Добавление в конец\n");
    for (size_t i = 0; i < 15; ++i)
    {
        ilappend(ilist, &source[i]);
    }

    ilprint(ilist);
    ilStructPrint(ilist);

    puts("Добавление в начало\n");
    for (size_t i = 15; i < 20; ++i)
    {
        ilappbegin(ilist, &source[i]);
    }

    ilprint(ilist);
    ilStructPrint(ilist);

    puts("Удаление элементов с 5-го по 15-ый\n");
    for (size_t i = 4; i < 15; ++i)
    {
        ilremove(ilist, 4);
    }

    ilprint(ilist);
    ilStructPrint(ilist);

    puts("Добавление в произвольную позицию\n");
    for (size_t i = 20; i < 25; ++i)
    {
        ilinsert(ilist, &source[i], (size_t)(rand() % 9));
    }

    ilprint(ilist);
    ilStructPrint(ilist);

    puts("Поиск некоторой записи из исходного массива\n");
    size_t source_rec_i = (rand() % 2)
            ? rand() % 5 + 10
            : rand() % 5;

    printf("Запись № %zu исходного массива находится на %d-ой позиции\n",
           source_rec_i,
           ilindex(ilist, &source[source_rec_i])
           );

    puts("Меняем первые 5 элементов списка на новые");
    for (size_t i = 0; i < 5; ++i)
    {
        size_t offset = 25;
        ilset(ilist, i, &source[offset + i]);
    }

    ilprint(ilist);
    ilStructPrint(ilist);

    puts("Очищаем список");
    ilclear(ilist);

    ilStructPrint(ilist);

    // Очистка памяти
    free(ilist);
    free(source);

    return 0;
}
